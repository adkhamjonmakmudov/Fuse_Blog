from ckeditor_uploader.fields import RichTextUploadingField
from django.contrib.auth.models import AbstractUser
from django.db.models import CharField, \
    Model, SlugField, ForeignKey, DateTimeField, CASCADE, ManyToManyField, TextChoices, SET_NULL, \
    IntegerField, TextField, EmailField, BooleanField, ImageField, Manager, PROTECT
from django.utils.text import slugify
from django_resized import ResizedImageField


class User(AbstractUser):
    phone = CharField(max_length=25, blank=True)
    bio = TextField(null=True, blank=True)
    email = EmailField(max_length=255, unique=False)
    is_active = BooleanField(default=False)
    image = ImageField( default='media/profile/default.jpg', upload_to='profile/')

    class Meta:
        verbose_name_plural = 'Userlar'  # noqa


# Create your models here.

class Category(Model):
    name = CharField(max_length=255)
    image = ResizedImageField(size=[400, 250], crop=['middle', 'center'], upload_to='category/')
    slug = SlugField(max_length=255, unique=True)

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.name)  # NOQA
            while Post.objects.filter(slug=self.slug).exists():
                slug = Post.objects.filter(slug=self.slug).first().slug
                if '-' in slug:
                    try:
                        if slug.split('-')[-1] in self.name:
                            self.slug += '-1'
                        else:
                            self.slug = '-'.join(slug.split('-')[:-1]) + '-' + str(int(slug.split('-')[-1]) + 1)
                    except:
                        self.slug = slug + '-1'
                else:
                    self.slug += '-1'
            super().save(*args, **kwargs)

    @property
    def post_count(self):
        return self.post_set.count()

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Kategoriya'  # noqa
        verbose_name_plural = 'Kategoriyalar'  # noqa


class ActivePostsManager(Manager):
    def get_queryset(self):
        return super().get_queryset().filter(status=Post.Status.ACTIVE)


class Post(Model):
    class Status(TextChoices):
        PENDING = 'pending', 'Kutilyapti'  # noqa
        ACTIVE = 'active', 'Aktiv'  # noqa
        CANCEL = 'cancel', 'Qabul qlinmadi'  # noqa

    title = CharField(max_length=255)
    slug = SlugField(max_length=255, unique=True)
    content = RichTextUploadingField()
    status = CharField(max_length=25, choices=Status.choices, default=Status.PENDING)
    author = ForeignKey(User, SET_NULL, null=True, blank=True)
    image = ResizedImageField(size=[600, 700], crop=['middle', 'center'], upload_to='posts/')
    view = IntegerField(default=0)
    category = ManyToManyField(Category)
    created_at = DateTimeField(auto_now=True)
    objects = Manager()
    active = ActivePostsManager()

    def save(self, *args, **kwargs):
        if not self.slug:
            self.slug = slugify(self.title)  # noqa
            while Post.objects.filter(slug=self.slug).exists():
                slug = Post.objects.filter(slug=self.slug).first().slug
                if '-' in slug:
                    try:
                        if slug.split('-')[-1] in self.title:
                            self.slug += '-1'
                        else:
                            self.slug = '-'.join(slug.split('-')[:-1]) + '-' + str(int(slug.split('-')[-1]) + 1)
                    except:
                        self.slug = slug + '-1'
                else:
                    self.slug += '-1'
        super().save(*args, **kwargs)

    class Meta:
        verbose_name_plural = 'Postlar'
        ordering = ('-created_at',)

    def __str__(self):
        return self.title

    # @property
    # def comment_count(self):
    #     return self.comment_set.count()
