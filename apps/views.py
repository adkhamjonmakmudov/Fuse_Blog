from django.contrib import messages
from django.contrib.auth import login
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView
from django.contrib.sites.shortcuts import get_current_site
from django.http import HttpResponse, JsonResponse
from django.shortcuts import redirect, render
from django.urls import reverse_lazy
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.views import View
from django.views.generic import ListView, TemplateView, DetailView, FormView, UpdateView

from apps.forms import CustomLoginForm, RegisterForm, CreatePostForm, ProfileForm
from apps.models import Category, Post, User
from apps.utils.tasks import send_to_gmail
from apps.utils.token import account_activation_token


# Create your views here.

class SearchingView(View):
    def post(self, request, *args, **kwargs):
        like = request.POST.get('find')
        data = {
            'posts': list(Post.objects.filter(title__icontains=like).values('title', 'image', 'slug')),
            'domain': get_current_site(request)
        }
        return JsonResponse(data)


class IndexView(ListView):
    queryset = Category.objects.all()
    context_object_name = 'categories'
    template_name = 'apps/basic/index.html'

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['last_post'] = Post.active.first()
        if Post.active.count() > 1:
            context['last_posts'] = Post.active.all()[1:5]
        return context


class PostListView(ListView):
    queryset = Post.objects.filter(status__iexact=Post.Status.ACTIVE).order_by('-created_at')
    template_name = 'apps/basic/blog-category.html'
    context_object_name = 'posts'
    paginate_by = 3

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        # slug = self.request.GET.get('category')
        context['categories'] = Category.objects.all()
        return context

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset()
        if category := self.request.GET.get('category'):
            return qs.filter(category__slug=category)
        return qs


class AboutView(TemplateView):
    template_name = 'apps/basic/about.html'


class ContactView(TemplateView):
    template_name = 'apps/basic/contact.html'


class DetailFormPostView(DetailView):
    model = Post
    template_name = 'apps/basic/post.html'
    queryset = Post.objects.all()
    context_object_name = 'post'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['post'] = Post.objects.filter(slug=self.kwargs.get('slug')).first()
        return context


class CustomLoginView(LoginView):
    form_class = CustomLoginForm
    template_name = 'apps/auth/login.html'
    fields = '__all__'
    redirect_authenticated_user = True

    def get_success_url(self):
        return reverse_lazy('index')


class CreatePostView(LoginRequiredMixin, FormView):
    form_class = CreatePostForm
    template_name = 'apps/basic/create_post.html'
    success_url = reverse_lazy('index')

    def get_context_data(self, *, object_list=None, **kwargs):
        context = super().get_context_data(object_list=object_list, **kwargs)
        context['categories'] = Category.objects.all()
        return context

    def form_valid(self, form):
        form.instance.author = self.request.user
        form.save()
        return super().form_valid(form)

    def form_invalid(self, form):
        return super().form_invalid(form)


class ProfileView(LoginRequiredMixin, UpdateView):
    template_name = 'apps/auth/profile.html'
    queryset = User.objects.all()
    success_url = reverse_lazy('profile')
    form_class = ProfileForm

    def get_object(self, queryset=None):
        return self.request.user

    def get(self, request, **kwargs):
        if self.request.user.is_anonymous:
            return redirect('login')
        self.object = self.request.user
        context = self.get_context_data(object=self.object, form=self.form_class)
        return self.render_to_response(context)

    def form_valid(self, form):
        form.save()
        return super().form_valid(form)


class RegisterView(FormView):
    template_name = 'apps/auth/register.html'
    form_class = RegisterForm
    redirect_authenticated_user = True
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        current_site = get_current_site(self.request)
        send_to_gmail.apply_async(args=[form.data.get('email'), current_site.domain])
        user = form.save()
        if user:
            login(self.request, user)
        return super().form_valid(form)

    def get(self, request, *args, **kwargs):
        if request.user.is_authenticated:
            return redirect('login')
        return super().get(request, *args, **kwargs)


class ActivateEmailView(TemplateView):
    template_name = 'apps/auth/confirm_mail.html'

    def get(self, request, *args, **kwargs):
        uid = kwargs.get('uid')
        token = kwargs.get('token')

        try:
            uid = force_str(urlsafe_base64_decode(uid))
            user = User.objects.get(pk=uid)
        except Exception as e:
            user = None
        if user and account_activation_token.check_token(user, token):
            user.is_active = True
            user.save()
            login(request, user, backend='django.contrib.auth.backends.ModelBackend')
            messages.add_message(
                request=request,
                level=messages.SUCCESS,
                message="Your account successfully activated!"
            )
            return redirect('login')
        else:
            return HttpResponse('Activation link is invalid!')
