from apps.models import Post


def use_full_models(request):
    return {
        "feature_posts": Post.objects.order_by('-created_at')[:3],
    }