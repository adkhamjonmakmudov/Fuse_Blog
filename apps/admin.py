from django.contrib import admin
from django.contrib.admin import ModelAdmin
from django.http import HttpResponseRedirect
from django.shortcuts import redirect
from django.urls import reverse, path, reverse_lazy
from django.utils.html import format_html
from django.utils.safestring import mark_safe

from apps.models import Category, Post, User


@admin.register(Category)
class CategoryAdmin(ModelAdmin):
    list_display = ('name', 'post_count', 'rasmi')  # noqa
    exclude = ('slug',)

    def rasmi(self, obj: Category):  # noqa
        return format_html(f'<img style="border-radius: 5px;" width="70px" height="65px" src="{obj.image.url}"/>')


@admin.register(User)
class CategoryAdmin(ModelAdmin):
    list_display = ('username', 'email', 'image')  # noqa
    exclude = ()


@admin.register(Post)
class PostAdmin(ModelAdmin):
    list_display = ('title', 'is_active', 'rasmi', 'created_at', 'Aktivatsiya')  # noqa
    exclude = ('view', 'slug')

    def Aktivatsiya(self, obj):  # noqa
        active = reverse_lazy('admin:active', kwargs={'id': obj.pk})
        cancel = reverse_lazy('admin:cancel', kwargs={'id': obj.pk})
        prev = reverse_lazy('admin:prev', kwargs={'slug': obj.slug})
        buttons = f'<a href="{active}" class="fa-solid fa-check default" style="color: green; font-size: 1.8em;margin-top: 20px; margin: auto;"></a>' \
                  f'<a href="{cancel}" class="fa-solid fa-circle-xmark default" style="color: red; font-size: 1.8em;margin-top: 8px; margin: auto;"></a>' \
                  f'<a href="{reverse("post", kwargs={"slug": obj.slug})}" class="fa-solid fa-eye"  style="color: gray; font-size: 1.8em;margin-top: 8px; margin: auto;"></a>'
        return mark_safe(buttons)

    def response_change(self, request, obj):
        post = request.POST
        if "_cancel" in post:
            self.get_queryset(request).filter(pk=obj.pk).update(status='cancel')
            self.message_user(request, "Post Bekor Qlindi !!!.")  # noqa
            return HttpResponseRedirect('/admin/apps/post/')
        elif '_active' in post:
            self.get_queryset(request).filter(pk=obj.pk).update(status='active')
            self.message_user(request, " Post Aktivlashtrildi.")  # noqa
            return HttpResponseRedirect('/admin/apps/post/')
        elif '_preview' in post:
            return redirect('post', slug=obj.slug)
        return super().response_change(request, obj)

    def get_urls(self):
        urls = super().get_urls()
        my_urls = [path('cancel/<int:id>', self.cancel, name='cancel'),
                   path('active/<int:id>', self.active, name='active'),
                   path('post/<str:slug>', self.prev, name='prev'),
                   ]
        return my_urls + urls

    def active(self, request, id):
        post = Post.objects.filter(pk=id).first()
        post.status = Post.Status.ACTIVE
        post.save()
        return HttpResponseRedirect('../')

    def cancel(self, request, id):
        post = Post.objects.filter(pk=id).first()
        post.status = Post.Status.CANCEL
        post.save()
        return HttpResponseRedirect('../')

    def prev(self, request, slug):
        post = Post.objects.filter(slug=slug).first()
        return format_html(f'''<a href="{reverse('post', args=(post.slug))}">{post.name}</a>''')

    def is_active(self, obj):
        data = {
            'pending': '<i class="fa-solid fa-hourglass-start" style="color: grey; font-size: 1.8em;margin-top: 8px; margin: auto;"style="color : yellow "></i>',
            # noqa
            'active': '<i class="fa-solid fa-check" style="color: green; font-size: 1.8em;margin-top: 8px; margin: auto;"style="color : green "></i>',
            # noqa
            'cancel': '<i class="fa-solid fa-circle-xmark"  style="color: red; font-size: 1.8em;margin-top: 8px; margin: auto;" style="color : red "></i>'
            # noqa
        }
        return format_html(data[obj.status])

    def categories(self, obj: Post):  # noqa
        lst = []
        for i in obj.category.all():
            lst.append(f'''<a href="{reverse('admin:apps_category_change', args=(i.pk))}">{i.name}</a>''')
        return format_html(', '.join(lst))

    def rasmi(self, obj: Post):
        return format_html(f'<img style="border-radius: 5px;" width="70px" height="65px" src="{obj.image.url}"/>')

    is_active.short_description = 'Status'



