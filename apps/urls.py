from django.conf.urls.static import static
from django.contrib.auth.views import LogoutView
from django.urls import path
from django.views.decorators.csrf import csrf_exempt

from apps.views import AboutView, ContactView, IndexView, PostListView, DetailFormPostView, CustomLoginView, \
    RegisterView, CreatePostView, ProfileView, ActivateEmailView, SearchingView
from root.settings import STATIC_ROOT, STATIC_URL

urlpatterns = [
                  path('', IndexView.as_view(), name='index'),
                  path('login', CustomLoginView.as_view(next_page='index'), name='login'),
                  path('register/', RegisterView.as_view(), name='register'),
                  path('logout', LogoutView.as_view(next_page='index'), name='logout'),
                  path('category', PostListView.as_view(), name='blog'),
                  path('post/<str:slug>', DetailFormPostView.as_view(), name='post'),
                  path('profile/<int:pk>', ProfileView.as_view(), name='profile'),
                  path('about', AboutView.as_view(), name='about'),
                  path('create_post', CreatePostView.as_view(), name='create_post'),
                  path('contact', ContactView.as_view(), name='contact'),
                  path('activate/<str:uid>/<str:token>', ActivateEmailView.as_view(), name='confirm_mail'),
                  path('logout', LogoutView.as_view(next_page='index'), name='logout'),
                  path('profile', ProfileView.as_view(), name='profile'),
] + static(STATIC_ROOT, document_root=STATIC_URL)
